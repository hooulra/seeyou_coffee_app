import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_margin_horizon.dart';
import 'package:see_you_app/config/config_size.dart';
import 'package:see_you_app/config/config_style.dart';
import 'package:see_you_app/enums/enum_size.dart';

class ComponentCountTitle extends StatelessWidget {
  final IconData icon;
  final int count;
  final String unitName;
  final String itemName;

  const ComponentCountTitle(this.icon, this.count, this.unitName, this.itemName,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: bodyPaddingAll,
      child: Row(
        children: [
          Icon(
            icon,
            size: 20,
          ),
          const ComponentMarginHorizon(
            enumSize: EnumSize.small,
          ),
          Text(
            '총 ${count.toString()}$unitName의 $itemName이(가) 있습니다.',
            style: const TextStyle(
              fontSize: fontSizeSm,
            ),
          ),
        ],
      ),
    );
  }
}

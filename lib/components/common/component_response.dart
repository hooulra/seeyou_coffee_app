import 'package:flutter/material.dart';
import 'package:see_you_app/config/config_size.dart';

class ComponentResponse extends StatefulWidget {
  const ComponentResponse({super.key, required this.title, required this.content});

  final String title;
  final String content;

  @override
  State<ComponentResponse> createState() => _ComponentResponseState();
}

class _ComponentResponseState extends State<ComponentResponse> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          widget.title,
          style: const TextStyle(
            fontSize: fontSizeBig,
          ),
        ),
        Text(
          widget.content,
          style: const TextStyle(
            fontSize: fontSizeBig,
          ),
        ),
      ],
    );
  }
}

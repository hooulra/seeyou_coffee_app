import 'package:dio/dio.dart';
import 'package:see_you_app/config/config_api.dart';
import 'package:see_you_app/functions/token_lib.dart';
import 'package:see_you_app/model/admin_money_history/money_detail_item_result.dart';
import 'package:see_you_app/model/admin_money_history/money_list_result.dart';

class RepoMoney {
  Future<MoneyDetailItemResult> getBoard(int boardId) async {
    final String baseUrl = '$apiUri/message-board/detail/history-id/{boardId}';

    // http://34.64.75.6:8080/v1/message-board/detail/history-id/1

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response =
        await dio.get(baseUrl.replaceAll('{boardId}', boardId.toString()),
            options: Options(
                followRedirects: false,
                validateStatus: (status) {
                  return status == 200;
                }));
    return MoneyDetailItemResult.fromJson(response.data);
  }

  // 다트문법. 페이지 언급안하면 기본값 1 주고싶을때.. {} 여기 안에 이렇게 쓰면 됨.
  // 자바에선 안됨.. 다트가 더 늦게 나온 언어라서 기존 언어 단점이 보완된거임..불편하니까..
  Future<MoneyListResult> getList({int page = 1}) async {
    final String _baseUrl = '$apiUri/message-board/list?page={page}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
      _baseUrl.replaceAll('{page}', page.toString()),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }),
    );
    return MoneyListResult.fromJson(response.data);
  }
}

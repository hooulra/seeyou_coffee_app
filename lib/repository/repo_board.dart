import 'package:dio/dio.dart';
import 'package:see_you_app/config/config_api.dart';
import 'package:see_you_app/functions/token_lib.dart';
import 'package:see_you_app/model/admin_board/board_list_result.dart';
import 'package:see_you_app/model/admin_board/board_request.dart';
import 'package:see_you_app/model/admin_board/board_result.dart';
import 'package:see_you_app/model/admin_board/board_update_request.dart';
import 'package:see_you_app/model/common_result.dart';

class RepoBoard {

  Future<CommonResult> setBoard(BoardRequest request) async {
    const String baseUrl = '$apiUri/message-board/data';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }

  Future<BoardResult> getBoard(int boardId) async {
    final String baseUrl =
        '$apiUri/message-board/detail/history-id/{boardId}';

    // http://34.64.75.6:8080/v1/message-board/detail/history-id/1

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        baseUrl.replaceAll('{boardId}', boardId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return BoardResult.fromJson(response.data);
  }



  Future<CommonResult> putBoard(
      int boardId, BoardUpdateRequest request) async {
    const String baseUrl =
        '$apiUri/message-board/title-name-context-img/history-id/{boardId}';


    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl
            .replaceAll('{boardId}', boardId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              print(status);
              return status == 200;
            }));


    return CommonResult.fromJson(response.data);
  }




  // 다트문법. 페이지 언급안하면 기본값 1 주고싶을때.. {} 여기 안에 이렇게 쓰면 됨.
  // 자바에선 안됨.. 다트가 더 늦게 나온 언어라서 기존 언어 단점이 보완된거임..불편하니까..
  Future<BoardListResult> getList({int page = 1}) async {
    final String _baseUrl = '$apiUri/message-board/list?page={page}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );
    return BoardListResult.fromJson(response.data);
  }
}
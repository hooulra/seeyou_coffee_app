import 'dart:ffi';

class MoneyDetailItem {
  Long id;
  String member;
  String moneyType;
  num beforeMoney;
  // String context;

  MoneyDetailItem(this.id, this.member, this.moneyType, this.beforeMoney);

  factory MoneyDetailItem.fromJson(Map<String, dynamic> json) {
    return MoneyDetailItem(
      json['id'],
      json['member'],
      json['moneyType'],
      json['beforeMoney'],
      // json['context'],
    );
  }
}
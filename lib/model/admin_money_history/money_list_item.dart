import 'dart:ffi';

class MoneyListItem {
  Long id;
  String member;
  String moneyType;
  num beforeMoney;
  // String context;

  MoneyListItem(this.id, this.member, this.moneyType, this.beforeMoney);

  factory MoneyListItem.fromJson(Map<String, dynamic> json) {
    return MoneyListItem(
      json['id'],
      json['member'],
      json['moneyType'],
      json['beforeMoney'],
      // json['context'],
    );
  }
}
class BoardListItem {
  int id;
  String title;
  String content;
  String dateWrite;
  // String context;

  BoardListItem(this.id, this.title, this.content, this.dateWrite);

  factory BoardListItem.fromJson(Map<String, dynamic> json) {
    return BoardListItem(
      json['id'],
      json['title'],
      json['content'],
      json['dateWrite'],
      // json['context'],
    );
  }
}
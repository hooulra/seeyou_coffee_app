import 'package:see_you_app/model/admin_board/board_response.dart';

class BoardResult{
  BoardResponse data;
  bool isSuccess; // 성공 여부를 묻는 변수
  int code; // isSuccess 결과에 따른 코드 값을 담는 변수
  String msg; // isSuccess 결과에 따른 메세지를 담는 변수

  BoardResult(this.data, this.isSuccess, this.code, this.msg);

  factory BoardResult.fromJson(Map<String, dynamic> json) {
    return BoardResult(
        BoardResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}
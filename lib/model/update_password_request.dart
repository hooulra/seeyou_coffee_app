class UpdatePasswordRequest {
  String changePassword;
  String changePasswordRe;
  String currentPassword;

  UpdatePasswordRequest(this.changePassword, this.changePasswordRe, this.currentPassword);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['changePassword'] = changePassword;
    data['changePasswordRe'] = changePasswordRe;
    data['currentPassword'] = currentPassword;

    return data;
  }
}
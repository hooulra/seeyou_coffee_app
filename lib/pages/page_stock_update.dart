import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_custom_loading.dart';
import 'package:see_you_app/components/common/component_margin_vertical.dart';
import 'package:see_you_app/components/common/component_notification.dart';
import 'package:see_you_app/components/common/component_text_btn.dart';
import 'package:see_you_app/config/config_form_validator.dart';
import 'package:see_you_app/config/config_style.dart';
import 'package:see_you_app/model/stock_request.dart';
import 'package:see_you_app/pages/page_main.dart';
import 'package:see_you_app/repository/repo_stock.dart';
import 'package:see_you_app/styles/style_form_decoration.dart';

class PageStock extends StatefulWidget {
  const PageStock({super.key});

  @override
  State<PageStock> createState() => _PageStockState();
}

class _PageStockState extends State<PageStock> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _putStock(StockRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoStock().putStock(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '재고 수정 완료',
        subTitle: '재고 수정이 완료되었습니다.',
      ).call();

      Navigator.push(
          context, MaterialPageRoute(
          builder: (BuildContext context) => const PageMain()
      )
      );
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '재고 수정 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: "재고 수정",
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          margin: const EdgeInsets.all(20),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'stockQuantity',
                    decoration: StyleFormDecoration().getInputDecoration('재고수량'),
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'minQuantity',
                    decoration: StyleFormDecoration().getInputDecoration('최소기준수량'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  child: ComponentTextBtn(
                      text : '수정',
                          callback : () {
                    if(_formKey.currentState!.saveAndValidate()) {
                      StockRequest stockRequest = StockRequest(
                        _formKey.currentState!.fields['stockQuantity']!.value,
                        _formKey.currentState!.fields['minQuantity']!.value,
                      );
                    }
                  }),
                ),
              ],
            ),
          ),
        )
      ]
    );
  }
}
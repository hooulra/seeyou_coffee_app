// import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:see_you_app/components/common/component_text_btn.dart';
// import 'package:see_you_app/config/config_color.dart';
// import 'package:see_you_app/config/config_style.dart';
// import 'package:see_you_app/pages/page_qr_reader.dart';
//
// class PageAttendance extends StatefulWidget {
//   const PageAttendance({super.key});
//
//   @override
//   State<PageAttendance> createState() => _PageAttendanceState();
// }
//
// class _PageAttendanceState extends State<PageAttendance> {
//   Position? _currentPosition;
//   Position? posX;
//   Position? posY;
//
//   @override
//   Widget build(BuildContext context) {
//     return ListView(
//       children: [
//         Container(
//           padding: const EdgeInsets.all(20),
//           child: Container(
//             padding: const EdgeInsets.all(15),
//             decoration: BoxDecoration(
//               color: Colors.white,
//               border: Border.all(color: colorLightGray),
//               borderRadius: BorderRadius.circular(buttonRadius),
//             ),
//             child: Column(
//               mainAxisSize: MainAxisSize.min,
//               crossAxisAlignment: CrossAxisAlignment.stretch,
//               children: [
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     ComponentTextBtn(text : "출근",
//                             callback: () {Navigator.push(
//                       context,
//                       MaterialPageRoute(
//                           builder: (BuildContext context) => PageQrReader(
//                               posX: _currentPosition!.latitude,
//                               posY: _currentPosition!.longitude)),
//                     );}),
//                     ComponentTextBtn(text : "퇴근",
//                             callback: () {Navigator.push(
//                       context,
//                       MaterialPageRoute(
//                           builder: (BuildContext context) => PageQrReader(
//                               posX: _currentPosition!.latitude,
//                               posY: _currentPosition!.longitude)),
//                     );}),
//                   ],
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ],
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_count_title.dart';
import 'package:see_you_app/components/common/component_list_textline_item.dart';
import 'package:see_you_app/pages/page_stock.dart';

class PageStockLackList extends StatefulWidget {
  const PageStockLackList({super.key});

  @override
  State<PageStockLackList> createState() => _PageStockLackListState();
}

class _PageStockLackListState extends State<PageStockLackList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: "재고 부족 리스트",
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    // if (_totalItemCount > 0) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const ComponentCountTitle(Icons.attachment, 100, '건', '재고'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: 100,
            itemBuilder: (_, index) =>
                ComponentListTextLineItem(
                  title: '상품명',
                  isUseContent1Line: true,
                  content1Subject: '재고수량',
                  content1Text: '10',
                  isUseContent2Line: true,
                  content2Subject: '최소기준수량',
                  content2Text: '20',
                  voidCallback: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (BuildContext context) => PageStock()));
                  },
                ),
          )
        ],
      ),
    );
    // } else {
    //   // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
    //   return SizedBox(
    //     height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
    //     child: const ComponentNoContents(
    //       icon: Icons.history,
    //       msg: '재고가 없습니다.',
    //     ),
    //   );
    // }
  }
}
